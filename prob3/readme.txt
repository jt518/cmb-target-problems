PROB3A
--------------------------------------------------------------------------------

* The mesh is `mesh2.gen`. Needs to be scaled by 0.001 (mm -> m).

* 2-material (graphite, uranium) with heat conduction and uranium phase change.
  No induction heating or enclosure radiation.

* Graphite material (single phase) assigned to element blocks 2, 3, 4, 5.
  Density 1750; specific heat 1500; conductivity 100.

* (liquid) Uranium (2-phase) is assigned to element block 1.
  - solid gamma phase: density 17.5e3; specific heat 160.9; conductivity 46.05
  - liquid phase: density 17.5e3; specific heat 201.3; conductivity 46.05
  - Solidus temperature 1396 K
  - Liquidus temperature 1416 K
  - Smoothing radius of 10
  - Latent heat 35794

* Initial temperature 1500 for the uranium (block 1)

* Linear temperature profile in the graphite mold:
  T = 1200 at z = -0.075m to T = 1400 at z = 0.285m

* Boundary conditions

  * No heat flux on sides set 1 (symmetry planes)
  * HTC on side set 2: coef 50; ref temp 300
  * Simple radiation on side sets 20, 21, 22: emissivity 0.75, amb temp 300
  * No heat flux on side sets 10, 11, 23, 6

* Interface conditions

  - HTC on side set 30: coef 500
  - Gap radiation on sideset 40: emissivity 0.75
  - HTC on side sets 41, 42: coef 500
  - HTC on side sets 50, 51: coef 200
  - HTC on side set 24; coef 500 (mold-casting)

* Numerical parameters

  - relative temperature and enthalpy tolerance: 1e-3
  - absolute temperature and enthalpy tolerance: 0
  - initial time step: 1e-2
  - dt_grow = 5.0
  - dt_max = 60

* Start at t=0, output every 10 sec to t = 120, then every 60 sec to
  final time 600.
